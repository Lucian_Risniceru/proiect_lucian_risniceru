package wantsome.project;


import wantsome.project.db.DbInitService;
import wantsome.project.db.generators.dao.GeneratorsDAO;
import wantsome.project.db.generators.dto.GeneratorsFullDetailsDTO;
import wantsome.project.db.users.dao.UsersDAO;
import wantsome.project.db.users.dto.UsersDTO;

import java.util.List;

public class GeneratorManualTest {
    public static void main(String[] args) {

        DbInitService.createMissingTables();
        DbInitService.createDefaultGeneratorsTypes();
        DbInitService.createAdminUser();
        //   DbInitService.createDefaultGenerator();


        List<UsersDTO> loadUsers = UsersDAO.loadAllUsers();
        loadUsers.forEach(System.out::println);


        //GeneratorsTypesDTO gen = new GeneratorsTypesDTO(101,"GTQA TEST2","TEST", Category.LOW_COST);
        // GeneratorTypeDAO.insertGeneratorType(gen);
        //  List<GeneratorsTypesDTO> generators = GeneratorTypeDAO.loadAllGeneratorsTypes();
        //  generators.forEach(System.out::println);

/*
      GeneratorsDAO.insertGenerator(new GeneratorsDTO(
                  1,"GTQA.038.2022",4,
                  Date.valueOf("2022-05-31"),0,null,null,
                  ProductStatus.STOCK,null,null, ProductionPriority.LOW));


       GeneratorsDAO.insertGenerator(new GeneratorsDTO(
                2,"GTQA.012.2022",6,
                Date.valueOf("2022-5-27"),0,null,null,
                ProductStatus.STOCK,null,null, ProductionPriority.LOW));

       GeneratorsDAO.insertGenerator(new GeneratorsDTO(
                3,"GTQA.037.2022",4,
                Date.valueOf("2022-06-21"),0,null,null,
                ProductStatus.STOCK,null,null, ProductionPriority.MEDIUM));

       GeneratorsDAO.insertGenerator(new GeneratorsDTO(
                4,"GTQA.036.2022",4,
                Date.valueOf("2022-05-21"),0,null,null,
                ProductStatus.STOCK,null,null, ProductionPriority.HIGH));

       GeneratorsDAO.insertGenerator(new GeneratorsDTO(
                5,"GTQA.035.2022",4,
                Date.valueOf("2022-03-10"),0,null,null,
                ProductStatus.STOCK,null,null, ProductionPriority.LOW));


        GeneratorsDAO.insertGenerator(new GeneratorsDTO(
                6,"GTQA.114.2022",1,
                Date.valueOf("2021-02-15"),4,null,null,
                ProductStatus.RESERVED,Date.valueOf("2022-04-15"),null, ProductionPriority.LOW));

        GeneratorsDAO.insertGenerator(new GeneratorsDTO(
                7,"GTQA.001.2022",4,
                Date.valueOf("2022-12-31"),3,null,null,
                ProductStatus.RESERVED,Date.valueOf("2022-06-15"),null, ProductionPriority.MEDIUM));

        GeneratorsDAO.insertGenerator(new GeneratorsDTO(
                8,"GTQA.030.2022",4,
                Date.valueOf("2021-08-31"),3,null,null,
                ProductStatus.RESERVED,Date.valueOf("2022-07-15"),null, ProductionPriority.HIGH));

        GeneratorsDAO.insertGenerator(new GeneratorsDTO(
                10,"GTQA.100.2022",20,
                Date.valueOf("2021-09-12"),3,null,null,
                ProductStatus.RESERVED,Date.valueOf("2022-11-01"),null, ProductionPriority.LOW));

        GeneratorsDAO.insertGenerator(new GeneratorsDTO(
                11,"GTQA.101.2022",21,
                Date.valueOf("2021-11-01"),3,null,null,
                ProductStatus.RESERVED,Date.valueOf("2022-05-26"),null, ProductionPriority.LOW));

        GeneratorsDAO.insertGenerator(new GeneratorsDTO(
                12,"GTQA.102.2022",22,
                Date.valueOf("2021-05-23"),3,null,null,
                ProductStatus.RESERVED,Date.valueOf("2022-02-13"),null, ProductionPriority.LOW));

        GeneratorsDAO.insertGenerator(new GeneratorsDTO(
                13,"GTQA.202.2022",22,
                Date.valueOf("2021-06-21"),3,"Promelek","De livrat cu ATS",
                ProductStatus.READY_FOR_SHIPMENT,Date.valueOf("2022-08-16"),Date.valueOf("2022-03-03"), ProductionPriority.LOW));

        GeneratorsDAO.insertGenerator(new GeneratorsDTO(
                14,"GTQA.203.2022",5,
                Date.valueOf("2021-05-01"),3,"Eltech",null,
                ProductStatus.READY_FOR_SHIPMENT,Date.valueOf("2022-06-05"),Date.valueOf("2022-07-07"), ProductionPriority.LOW));

        GeneratorsDAO.insertGenerator(new GeneratorsDTO(
                15,"GTQA.204.2022",22,
                Date.valueOf("2021-06-05"),3,"Promelek","De livrat cu ATS",
                ProductStatus.READY_FOR_SHIPMENT,Date.valueOf("2022-06-08"),Date.valueOf("2022-07-05"), ProductionPriority.LOW));

        GeneratorsDAO.insertGenerator(new GeneratorsDTO(
                16,"GTQA.205.2022",5,
                Date.valueOf("2021-11-01"),3,"Domitech",null,
                ProductStatus.READY_FOR_SHIPMENT,Date.valueOf("2022-06-25"),Date.valueOf("2022-07-11"), ProductionPriority.LOW));

*/


        List<GeneratorsFullDetailsDTO> loadFullDetails = GeneratorsDAO.loadFullDetailsGenerators();
        loadFullDetails.forEach(System.out::println);

    }
}
