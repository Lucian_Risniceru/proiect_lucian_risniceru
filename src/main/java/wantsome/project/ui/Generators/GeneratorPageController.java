package wantsome.project.ui.Generators;

import io.javalin.http.Context;
import wantsome.project.db.generators.dao.GeneratorsDAO;
import wantsome.project.db.generators.dto.GeneratorsFullDetailsDTO;
import wantsome.project.db.generators.dto.ProductStatus;
import wantsome.project.ui.users.UsersLoginController;

import java.sql.Date;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GeneratorPageController {
    public enum Order {
        CATEGORY, MANUFACTURING_DATE, PRIORITY
    }


    public static void showGeneratorsPage(Context ctx) {

        List<GeneratorsFullDetailsDTO> allGenerators = GeneratorsDAO.loadFullDetailsGenerators();

        ProductStatus filter = getFilterFromQueryOrSes(ctx);
        Order order = getOrderFromQueryOrSes(ctx);
        List<GeneratorsFullDetailsDTO> generators = getGeneratorsToDisplay(allGenerators, filter, order);

        long stockCount = allGenerators.stream().filter(i -> i.getProduct_status() == ProductStatus.STOCK).count();
        long reservedCount = allGenerators.stream().filter(i -> i.getProduct_status() == ProductStatus.RESERVED).count();
        long inProgressCount = allGenerators.stream().filter(i -> i.getProduct_status() == ProductStatus.IN_PROGRESS).count();


        Map<String, Object> model = new HashMap<>();
        model.put("generators", generators);
        model.put("crtFilter", filter);
        model.put("crtOrder", order);
        model.put("stockCount", stockCount);
        model.put("reservedCount", reservedCount);
        model.put("inProgressCount", inProgressCount);
        UsersLoginController.addUserInfoToModel(ctx, model);

        model.put("showProduction", true);

        ctx.render("generators.vm", model);
    }


    private static ProductStatus getFilterFromQueryOrSes(Context ctx) {
        String param = fromQueryOrSession(ctx, "filter");
        return param == null ? ProductStatus.STOCK :
                (param.equals("ALL") ?
                        null :
                        ProductStatus.valueOf(param));
    }

    private static String fromQueryOrSession(Context ctx, String name) {
        String param = ctx.queryParam(name);
        if (param != null) {
            ctx.sessionAttribute(name, param);
        } else {
            param = ctx.sessionAttribute(name);
        }
        return param;
    }

    private static Order getOrderFromQueryOrSes(Context ctx) {
        String param = fromQueryOrSession(ctx, "order");
        return param == null ?
                Order.CATEGORY :
                Order.valueOf(param);
    }

    private static List<GeneratorsFullDetailsDTO> getGeneratorsToDisplay(List<GeneratorsFullDetailsDTO> allGenerators, ProductStatus productStatus, Order order) {
        return allGenerators.stream()
                .filter(n -> productStatus == null || n.getProduct_status() == productStatus)
                .sorted(getDisplayComparator(order))
                .collect(Collectors.toList());
    }

    private static Comparator<GeneratorsFullDetailsDTO> getDisplayComparator(Order order) {
        if (order == Order.CATEGORY) {
            return Comparator.comparing(GeneratorsFullDetailsDTO::getCategory).reversed();
        } else {
            Date defaultDate = new Date(0);
            return (i1, i2) -> {
                Date d1 = i1.getManufacturing_date() != null ? i1.getManufacturing_date() : defaultDate;
                Date d2 = i2.getManufacturing_date() != null ? i2.getManufacturing_date() : defaultDate;
                return d1.compareTo(d2);
            };
        }
    }

    public static void handleDeleteGeneratorRequest(Context ctx) {
        int id = ctx.pathParamAsClass("id", Integer.class).get();
        GeneratorsDAO.deleteGenerator(id);

        ctx.redirect("/generators");
    }
}
