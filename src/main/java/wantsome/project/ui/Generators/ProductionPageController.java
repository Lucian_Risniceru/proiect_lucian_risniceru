package wantsome.project.ui.Generators;

import io.javalin.core.validation.JavalinValidation;
import io.javalin.http.Context;
import wantsome.project.db.generators.dao.GeneratorsDAO;
import wantsome.project.db.generators.dto.GeneratorsFullDetailsDTO;
import wantsome.project.db.generators.dto.ProductStatus;
import wantsome.project.db.generators.dto.ProductionPriority;
import wantsome.project.db.generators_types.dto.Category;

import java.sql.Date;
import java.util.*;
import java.util.stream.Collectors;


public class ProductionPageController {


    static {
        JavalinValidation.register(Date.class, s -> s != null && !s.isEmpty() ? Date.valueOf(s) : null);
        JavalinValidation.register(ProductStatus.class, s -> s != null && !s.isEmpty() ? ProductStatus.valueOf(s) : null);
        JavalinValidation.register(Category.class, s -> s != null && !s.isEmpty() ? Category.valueOf(s) : null);
        JavalinValidation.register(ProductionPriority.class, s -> s != null && !s.isEmpty() ? ProductionPriority.valueOf(s) : null);
    }


    public static void showInProgressGenerators(Context ctx) {

        List<GeneratorsFullDetailsDTO> inProgressGenerators = GeneratorsDAO.loadInProgressGenerators();

        ProductionPriority filter = getFilterFromQueryOrSes(ctx);
        GeneratorPageController.Order order = getOrderFromQueryOrSes(ctx);
        List<GeneratorsFullDetailsDTO> generators = getGeneratorsToDisplay(inProgressGenerators, filter, order);

        Map<String, Object> model = new HashMap<>();

        model.put("generators", generators);
        model.put("crtFilter", filter);
        model.put("crtOrder", order);
        model.put("currentUser", ctx.sessionAttribute("currentUser"));

        ctx.render("in_progress_generators.vm", model);
    }

    private static ProductionPriority getFilterFromQueryOrSes(Context ctx) {
        String param = ctx.queryParam("filter");
        return param == null ? null :
                (param.equals("ALL") ?
                        null :
                        ProductionPriority.valueOf(param));
    }

    private static GeneratorPageController.Order getOrderFromQueryOrSes(Context ctx) {
        String param = ctx.queryParam("order");
        return param == null ?
                GeneratorPageController.Order.PRIORITY :
                GeneratorPageController.Order.valueOf(param);
    }

    private static List<GeneratorsFullDetailsDTO> getGeneratorsToDisplay(List<GeneratorsFullDetailsDTO> inProgressGenerators, ProductionPriority priority, GeneratorPageController.Order order) {
        return inProgressGenerators.stream()
                .filter(n -> priority == null || n.getProduction_priority() == priority)
                .sorted(getDisplayComparator(order))
                .collect(Collectors.toList());
    }

    private static Comparator<GeneratorsFullDetailsDTO> getDisplayComparator(GeneratorPageController.Order order) {
        return order == GeneratorPageController.Order.PRIORITY ?
                Comparator.comparing(GeneratorsFullDetailsDTO::getProduction_priority) :
                Comparator.comparing(GeneratorsFullDetailsDTO::getManufacturing_date,
                        Comparator.nullsFirst(Comparator.naturalOrder()));
    }

    public static void showUpdateStatusInProgressForm(Context ctx) {

        Integer id = ctx.pathParamAsClass("id", Integer.class).get();
        Optional<GeneratorsFullDetailsDTO> optGenerators = GeneratorsDAO.load(id);
        GeneratorsFullDetailsDTO generators = optGenerators.orElseThrow(() -> new RuntimeException("Generator with id " + id + "not found!"));

        renderChangeProductionPriorityStatusForm(ctx,
                generators.getId(),
                generators.getManufacturing_date(),
                generators.getProduct_serial_number(),
                generators.getProduct_type(),
                generators.getCapacity(),
                generators.getCategory(),
                generators.getFirst_name(),
                generators.getLast_name(),
                generators.getCustomer(),
                generators.getRemark(),
                generators.getProduct_status(),
                generators.getReservation_date(),
                generators.getShipping_date(),
                generators.getProduction_priority(),
                "");
    }

    private static void renderChangeProductionPriorityStatusForm(Context ctx,
                                                                 int id,
                                                                 java.sql.Date manufacturing_date,
                                                                 String product_serial_number,
                                                                 String product_type,
                                                                 String capacity,
                                                                 Category category,
                                                                 String first_name,
                                                                 String last_name,
                                                                 String customer,
                                                                 String remark,
                                                                 ProductStatus product_status,
                                                                 java.sql.Date reservation_date,
                                                                 Date shipping_date,
                                                                 ProductionPriority production_priority,
                                                                 String error) {

        Map<String, Object> model = new HashMap<>();

        model.put("prevId", id);
        model.put("prevManufacturingDate", manufacturing_date);
        model.put("prevProductSerialNumber", product_serial_number);
        model.put("prevProductType", product_type);
        model.put("prevCapacity", capacity);
        model.put("prevCategory", category);
        model.put("prevFirstName", first_name);
        model.put("prevLastName", last_name);
        model.put("prevCustomer", customer);
        model.put("prevRemark", remark);
        model.put("prevProductStatus", product_status);
        model.put("prevReservationDate", reservation_date);
        model.put("prevShippingDate", shipping_date);
        model.put("prevProductionPriority", production_priority);

        model.put("error", error);

        List<ProductStatus> productStatusList;
        if (product_status == ProductStatus.IN_PROGRESS) {
            productStatusList = List.of(ProductStatus.IN_PROGRESS, ProductStatus.STOCK);
        } else {
            productStatusList = List.of(product_status);
        }
        model.put("ProductStatusOption", productStatusList);
        model.put("ProductTypeOption", Category.values());
        model.put("ProductionPriorityOption", ProductionPriority.values());


        ctx.render("update_in_progress_generator.vm", model);
    }


    public static void handleUpdateInProgressPriorityRequest(Context ctx) {
        int id = ctx.formParamAsClass("id", Integer.class).getOrDefault(-1);
        Date manufacturing_date = ctx.formParamAsClass("manufacturing_date", Date.class).allowNullable().get();
        String product_serial_number = ctx.formParam("product_serial_number");
        String product_type = ctx.formParam("product_type");
        String capacity = ctx.formParam("capacity");
        Category category = ctx.formParamAsClass("category", Category.class).allowNullable().get();
        String first_name = ctx.formParam("first_name");
        String last_name = ctx.formParam("last_name");
        String customer = ctx.formParam("customer");
        String remark = ctx.formParam("remark");
        ProductStatus product_status = ctx.formParamAsClass("product_status", ProductStatus.class).get();
        Date reservation_date = ctx.formParamAsClass("reservation_date", Date.class).allowNullable().get();
        Date shipping_date = ctx.formParamAsClass("shipping_date", Date.class).allowNullable().get();
        ProductionPriority production_priority = ctx.formParamAsClass("production_priority", ProductionPriority.class).get();

        try {
            GeneratorsFullDetailsDTO inProgressGenerator = new GeneratorsFullDetailsDTO(id, manufacturing_date,
                    product_serial_number, product_type, capacity, category, first_name, last_name, customer,
                    remark, product_status, reservation_date, shipping_date, production_priority);

            GeneratorsDAO.updateStatusProductionPriority(inProgressGenerator);

            ctx.redirect("/generators/inProgressGenerators");

        } catch (Exception e) {
            renderChangeProductionPriorityStatusForm(ctx, id, manufacturing_date,
                    product_serial_number, product_type, capacity, category, first_name, last_name, customer,
                    remark, product_status, reservation_date, shipping_date, production_priority, e.getMessage());
        }

    }

    public static void handleDeleteGeneratorRequest(Context ctx) {
        int id = ctx.pathParamAsClass("id", Integer.class).get();
        GeneratorsDAO.deleteGenerator(id);

        ctx.redirect("/generators/inProgressGenerators");
    }
}
