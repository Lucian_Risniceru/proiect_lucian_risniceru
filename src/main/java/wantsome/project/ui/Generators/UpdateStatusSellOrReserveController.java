package wantsome.project.ui.Generators;


import io.javalin.core.validation.JavalinValidation;
import io.javalin.http.Context;
import wantsome.project.db.generators.dao.GeneratorsDAO;
import wantsome.project.db.generators.dto.GeneratorsDTO;
import wantsome.project.db.generators.dto.GeneratorsFullDetailsDTO;
import wantsome.project.db.generators.dto.ProductStatus;
import wantsome.project.db.generators.dto.ProductionPriority;
import wantsome.project.db.generators_types.dto.Category;
import wantsome.project.db.users.dto.UsersDTO;
import wantsome.project.ui.users.UsersLoginController;

import java.sql.Date;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;


public class UpdateStatusSellOrReserveController {

    static {
        JavalinValidation.register(Date.class, s -> s != null && !s.isEmpty() ? Date.valueOf(s) : null);
        JavalinValidation.register(ProductStatus.class, s -> s != null && !s.isEmpty() ? ProductStatus.valueOf(s) : null);
        JavalinValidation.register(Category.class, s -> s != null && !s.isEmpty() ? Category.valueOf(s) : null);
        JavalinValidation.register(ProductionPriority.class, s -> s != null && !s.isEmpty() ? ProductionPriority.valueOf(s) : null);

    }

    public static void showSellOrReserveForm(Context ctx) {

        int id = ctx.pathParamAsClass("id", Integer.class).get();
        Optional<GeneratorsFullDetailsDTO> optGenerators = GeneratorsDAO.load(id);
        GeneratorsFullDetailsDTO generators = optGenerators.orElseThrow(() -> new RuntimeException("Generator with id " + id + "not found!"));

        renderChangeStatusForm(ctx,
                generators.getId(),
                generators.getManufacturing_date(),
                generators.getProduct_serial_number(),
                generators.getProduct_type(),
                generators.getCapacity(),
                generators.getCategory(),
                generators.getCustomer(),
                generators.getRemark(),
                generators.getProduct_status(),
                generators.getReservation_date(),
                generators.getShipping_date(),
                generators.getProduction_priority(),
                "");
    }

    private static void renderChangeStatusForm(Context ctx,
                                               int id,
                                               Date manufacturing_date,
                                               String product_serial_number,
                                               String product_type,
                                               String capacity,
                                               Category category,
                                               String customer,
                                               String remark,
                                               ProductStatus product_status,
                                               Date reservation_date,
                                               Date shipping_date,
                                               ProductionPriority production_priority,
                                               String error) {

        Map<String, Object> model = new HashMap<>();

        model.put("prevId", id);
        model.put("prevManufacturingDate", manufacturing_date);
        model.put("prevProductSerialNumber", product_serial_number);
        model.put("prevProductType", product_type);
        model.put("prevCapacity", capacity);
        model.put("prevCategory", category);
        model.put("prevCustomer", customer);
        model.put("prevRemark", remark);
        model.put("prevProductStatus", product_status);
        model.put("prevReservationDate", reservation_date);
        model.put("prevShippingDate", shipping_date);
        model.put("prevProductionPriority", production_priority);
        model.put("daysUntilReservationDate", computeDaysUntilReservationRate(reservation_date));

        model.put("error", error);

        List<ProductStatus> productStatusList;
        if (product_status == ProductStatus.RESERVED || product_status == ProductStatus.STOCK) {
            productStatusList = List.of(ProductStatus.STOCK, ProductStatus.RESERVED, ProductStatus.READY_FOR_SHIPMENT);
        } else if (product_status == ProductStatus.READY_FOR_SHIPMENT) {
            productStatusList = List.of(ProductStatus.READY_FOR_SHIPMENT, ProductStatus.SHIPPED);
        } else {
            productStatusList = List.of(product_status);
        }
        model.put("ProductStatusOption", productStatusList);

        model.put("ProductTypeOption", Category.values());
        UsersLoginController.addUserInfoToModel(ctx, model);


        ctx.render("sell_or_reserve.vm", model);
    }


    public static long computeDaysUntilReservationRate(Date date) {
        if (date != null) {
            try {
                LocalDateTime today = LocalDate.now().atStartOfDay();
                LocalDateTime reservation_date = date.toLocalDate().atStartOfDay();
                return Duration.between(today, reservation_date).toDays();
            } catch (Exception e) {
                System.err.println("Error computing days until reservation date'" + date + "':" + e.getMessage());
            }
        }

        return 0;
    }

    public static void handleUpdateSellOrReservePostRequest(Context ctx) {
        int id = ctx.formParamAsClass("id", Integer.class).getOrDefault(-1);
        String product_serial_number = ctx.formParam("product_serial_number");
        int generator_type_id = ctx.formParamAsClass("generator_type_id", Integer.class).getOrDefault(-1);
        Date manufacturing_date = ctx.formParamAsClass("manufacturing_date", Date.class).allowNullable().get();
        String product_type = ctx.formParam("product_type");
        String capacity = ctx.formParam("capacity");
        Category category = ctx.formParamAsClass("category", Category.class).allowNullable().get();
        String customer = ctx.formParam("customer");
        String remark = ctx.formParam("remark");
        ProductStatus product_status = ctx.formParamAsClass("product_status", ProductStatus.class).get();
        Date reservation_date = ctx.formParamAsClass("reservation_date", Date.class).allowNullable().get();
        Date shipping_date = ctx.formParamAsClass("shipping_date", Date.class).allowNullable().get();
        ProductionPriority production_priority = ctx.formParamAsClass("production_priority", ProductionPriority.class).allowNullable().get();

        UsersDTO currentUser = ctx.sessionAttribute("currentUser");


        String serverAction = ctx.formParam("serverAction");
        if (Objects.equals(serverAction, "refresh")) {
            renderChangeStatusForm(ctx, id, manufacturing_date, product_serial_number,
                    product_type, capacity, category, customer, remark,
                    product_status, reservation_date, shipping_date, production_priority, null);
        } else {
            tryPerformUpdateAction(ctx, id, product_serial_number, generator_type_id, manufacturing_date, currentUser.getId(), customer,
                    remark, product_status, reservation_date, shipping_date, product_type, capacity, category, production_priority);
        }
    }

    private static void tryPerformUpdateAction(Context ctx,
                                               int id,
                                               String product_serial_number,
                                               int generator_type_id,
                                               Date manufacturing_date,
                                               int user_id,
                                               String customer,
                                               String remark,
                                               ProductStatus product_status,
                                               Date reservation_date,
                                               Date shipping_date,
                                               String product_type,
                                               String capacity,
                                               Category category,
                                               ProductionPriority production_priority) {
        try {
            GeneratorsDTO generator = validateAndBuildGenerator(id, product_serial_number,
                    generator_type_id, manufacturing_date, user_id, customer, remark, product_status, reservation_date, shipping_date, production_priority);
            GeneratorsDAO.updateSellOrReserveGenerator(generator);
            ctx.redirect("/generators");
        } catch (Exception e) {
            renderChangeStatusForm(ctx, id, manufacturing_date, product_serial_number,
                    product_type, capacity, category, customer, remark,
                    product_status, reservation_date, shipping_date, production_priority, e.getMessage());
        }
    }

    private static GeneratorsDTO validateAndBuildGenerator(int id,
                                                           String product_serial_number,
                                                           int generator_type_id,
                                                           Date manufacturing_date,
                                                           int user_id,
                                                           String customer,
                                                           String remark,
                                                           ProductStatus product_status,
                                                           Date reservation_date,
                                                           Date shipping_date,
                                                           ProductionPriority production_priority) {


        return new GeneratorsDTO(id, product_serial_number,
                generator_type_id, manufacturing_date, user_id, customer, remark,
                product_status, reservation_date, shipping_date, production_priority);
    }
}
