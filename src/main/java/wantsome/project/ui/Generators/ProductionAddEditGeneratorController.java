package wantsome.project.ui.Generators;

import io.javalin.core.validation.JavalinValidation;
import io.javalin.http.Context;
import wantsome.project.db.generators.dao.GeneratorsDAO;
import wantsome.project.db.generators.dto.GeneratorsDTO;
import wantsome.project.db.generators.dto.ProductStatus;
import wantsome.project.db.generators.dto.ProductionPriority;
import wantsome.project.db.generators_types.dao.GeneratorTypeDAO;
import wantsome.project.db.generators_types.dto.Category;
import wantsome.project.db.users.dto.UsersDTO;
import wantsome.project.ui.users.UsersLoginController;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ProductionAddEditGeneratorController {
    static {
        JavalinValidation.register(Date.class, s -> s != null && !s.isEmpty() ? Date.valueOf(s) : null);
        JavalinValidation.register(ProductStatus.class, s -> s != null && !s.isEmpty() ? ProductStatus.valueOf(s) : null);
        JavalinValidation.register(Category.class, s -> s != null && !s.isEmpty() ? Category.valueOf(s) : null);
        JavalinValidation.register(ProductionPriority.class, s -> s != null && !s.isEmpty() ? ProductionPriority.valueOf(s) : null);
    }

    public static void showAddForm(Context ctx) {
        renderAddForm(ctx, -1, null, "",
                0, null, null, null, ProductionPriority.LOW, "");
    }

    private static void renderAddForm(Context ctx, int id, Date manufacturing_date,
                                      String product_serial_number, int generator_type_id,
                                      ProductStatus product_status, Date reservation_date,
                                      Date shipping_date, ProductionPriority production_priority, String error) {

        Map<String, Object> model = new HashMap<>();
        model.put("prevId", id);
        model.put("prevManufacturingDate", manufacturing_date);
        model.put("prevProductSerialNumber", product_serial_number);
        model.put("prevProductStatus", product_status);
        model.put("prevReservationDate", reservation_date);
        model.put("prevShippingDate", shipping_date);
        model.put("prevPriority", production_priority);

        model.put("error", error);

        List<ProductStatus> productStatusList;
        productStatusList = List.of(ProductStatus.STOCK, ProductStatus.IN_PROGRESS);
        model.put("ProductStatusOption", productStatusList);
        model.put("ProductTypeOption", GeneratorTypeDAO.loadAllGeneratorsTypes());
        model.put("ProductionPriorityOption", ProductionPriority.values());
        UsersLoginController.addUserInfoToModel(ctx, model);

        ctx.render("add_generator.vm", model);
    }

    public static void handleAddRequest(Context ctx) {
        int id = ctx.formParamAsClass("id", Integer.class).getOrDefault(-1);
        UsersDTO currentUser = ctx.sessionAttribute("currentUser");
        Date manufacturing_date = ctx.formParamAsClass("manufacturing_date", Date.class).get();
        String product_serial_number = ctx.formParam("product_serial_number");
        int generator_type_id = ctx.formParamAsClass("generator_type_id", Integer.class).get();
        ProductStatus product_status = ctx.formParamAsClass("product_status", ProductStatus.class).get();
        Date reservation_date = ctx.formParamAsClass("reservation_date", Date.class).allowNullable().get();
        Date shipping_date = ctx.formParamAsClass("shipping_date", Date.class).allowNullable().get();
        ProductionPriority production_priority = ctx.formParamAsClass("production_priority", ProductionPriority.class).get();

        String serverAction = ctx.formParam("serverAction");
        if (Objects.equals(serverAction, "refresh")) {
            renderAddForm(ctx, id, manufacturing_date, product_serial_number,
                    generator_type_id, product_status,
                    reservation_date, shipping_date, production_priority, "");
        } else {
            tryPerformAddAction(ctx, id, manufacturing_date, product_serial_number,
                    generator_type_id, currentUser.getId(), product_status, reservation_date,
                    shipping_date, production_priority);
        }
    }

    public static void tryPerformAddAction(Context ctx, int id, Date manufacturing_date,
                                           String product_serial_number,
                                           int generator_type_id,
                                           int user_id, ProductStatus product_status,
                                           Date reservation_date, Date shipping_date,
                                           ProductionPriority production_priority) {

        try {
            GeneratorsDTO generator = new GeneratorsDTO(id, product_serial_number,
                    generator_type_id, manufacturing_date, user_id, null, null,
                    product_status, reservation_date, shipping_date, production_priority);

            GeneratorsDAO.insertGenerator(generator);

            ctx.redirect("/generators/inProgressGenerators");
        } catch (Exception e) {
            renderAddForm(ctx, id, manufacturing_date, product_serial_number,
                    generator_type_id,
                    product_status, reservation_date, shipping_date,
                    production_priority, "Generator with same serial number already exist!");
        }
    }
}