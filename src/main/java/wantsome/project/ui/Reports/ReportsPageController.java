package wantsome.project.ui.Reports;

import io.javalin.http.Context;
import wantsome.project.db.generators.dao.GeneratorsDAO;
import wantsome.project.db.generators.dto.GeneratorsFullDetailsDTO;
import wantsome.project.db.generators.dto.ProductStatus;
import wantsome.project.db.generators_types.dao.GeneratorTypeDAO;
import wantsome.project.db.users.dao.UsersDAO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;


public class ReportsPageController {

    public static void showShippedGenerators(Context ctx) {
        renderShippedGenerators(ctx);
    }

    private static void renderShippedGenerators(Context ctx) {

        List<GeneratorsFullDetailsDTO> generators = GeneratorsDAO.loadFullDetailsGenerators();


        Map<String, Long> shippedCountByUser = generators.stream()
                .filter(n -> n.getProduct_status() == ProductStatus.SHIPPED)
                .collect(groupingBy(GeneratorsFullDetailsDTO::getFirst_name, counting()));

        Map<String, Object> model = new HashMap<>();

        model.put("generators", GeneratorsDAO.shippedGenerators());
        model.put("generators_types", GeneratorTypeDAO.outOfStock());
        model.put("top_sellers", UsersDAO.topSeller());


        model.put("shippedCountByUser", shippedCountByUser);
        model.put("currentUser", ctx.sessionAttribute("currentUser"));

        ctx.render("reports.vm", model);
    }


}



