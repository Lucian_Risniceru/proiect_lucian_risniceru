package wantsome.project.ui.users;

import io.javalin.core.validation.JavalinValidation;
import io.javalin.http.Context;
import wantsome.project.db.users.dao.UsersDAO;
import wantsome.project.db.users.dto.Department;
import wantsome.project.db.users.dto.UsersDTO;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;


public class UpdateCurrentUserDetailsController {
    static {
        JavalinValidation.register(Department.class, s -> s != null && !s.isEmpty() ? Department.valueOf(s) : null);
    }

    public static void showUpdateUserDetailsPage(Context ctx) {

        UsersDTO currentUser = ctx.sessionAttribute("currentUser");
        Optional<UsersDTO> user = UsersDAO.load(currentUser.getId());
        UsersDTO users = user.orElseThrow(() -> new RuntimeException("User not found"));

        renderUpdateForm(ctx,
                users.getFirst_name(),
                users.getLast_name(),
                users.getEmail(),
                users.getPhone(),
                users.getUsername(),
                users.getPassword(),
                null);
    }

    private static void renderUpdateForm(Context ctx,
                                         String first_name,
                                         String last_name,
                                         String email,
                                         String phone,
                                         String username,
                                         String password,
                                         String error) {

        Map<String, Object> model = new HashMap<>();

        model.put("prevFirstName", first_name);
        model.put("prevLastName", last_name);
        model.put("prevEmail", email);
        model.put("prevPhone", phone);
        model.put("prevUsername", username);
        model.put("prevPassword", password);

        model.put("error", error);

        UsersLoginController.addUserInfoToModel(ctx, model);

        ctx.render("update_user_details.vm", model);

    }

    public static void handleUpdateRequest(Context ctx) {
        String first_name = ctx.formParam("first_name");
        String last_name = ctx.formParam("last_name");
        String email = ctx.formParam("email");
        String phone = ctx.formParam("phone");
        String username = ctx.formParam("username");
        String password = ctx.formParam("password");

        UsersDTO currentUser = ctx.sessionAttribute("currentUser");

        String serverAction = ctx.formParam("serverAction");
        if (Objects.equals(serverAction, "refresh")) {
            renderUpdateForm(ctx,
                    first_name,
                    last_name,
                    email,
                    phone,
                    username,
                    password, null);
        } else {
            tryPerformUpdateAction(ctx, currentUser.getId(), first_name, last_name, email, phone, currentUser.getDepartment(), username, password);
        }
    }

    private static void tryPerformUpdateAction(Context ctx, int id,
                                               String first_name,
                                               String last_name,
                                               String email,
                                               String phone,
                                               Department department,
                                               String username,
                                               String password) {
        try {
            UsersDTO user = new UsersDTO(id, first_name, last_name, email, phone, department, username, password);
            UsersDAO.updatePersonalInfo(user);

            ctx.sessionAttribute("currentUser", user);

            ctx.redirect("/generators");

        } catch (Exception e) {
            renderUpdateForm(ctx, first_name, last_name, email, phone, username, password, "Username already exist!");
        }

    }
}
