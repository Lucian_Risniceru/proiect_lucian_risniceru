package wantsome.project.ui.users;

import io.javalin.http.Context;
import wantsome.project.db.users.dao.UsersDAO;
import wantsome.project.db.users.dto.Department;

import java.util.HashMap;
import java.util.Map;

public class UsersPageController {

    public static void showUsersPage(Context ctx) {
        renderAddUsersForm(ctx, null);

    }

    public static void renderAddUsersForm(Context ctx, String error) {
        Map<String, Object> model = new HashMap<>();

        model.put("users", UsersDAO.loadAllUsers());
        model.put("error", error);
        model.put("DepartmentOption", Department.values());
        UsersLoginController.addUserInfoToModel(ctx, model);

        ctx.render("users.vm", model);
    }

    public static void handleDeleteUserRequest(Context ctx) {
        try {
            int id = ctx.pathParamAsClass("id", Integer.class).get();
            UsersDAO.deleteUser(id);
            ctx.redirect("/users");
        } catch (Exception e) {
            renderAddUsersForm(ctx, "User can't be deleted, username in use!");
        }

    }
}
