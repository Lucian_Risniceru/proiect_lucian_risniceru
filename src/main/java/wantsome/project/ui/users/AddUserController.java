package wantsome.project.ui.users;

import io.javalin.core.validation.JavalinValidation;
import io.javalin.http.Context;
import wantsome.project.db.users.dao.UsersDAO;
import wantsome.project.db.users.dto.Department;
import wantsome.project.db.users.dto.UsersDTO;

import java.util.HashMap;
import java.util.Map;

public class AddUserController {

    static {
        JavalinValidation.register(Department.class, s -> s != null && !s.isEmpty() ? Department.valueOf(s) : null);
    }


    public static void showAddUserPage(Context ctx) {

        renderAddEditUserForm(ctx, -1, "", "", "", "", Department.SALES, "", "", null);
    }

    private static void renderAddEditUserForm(Context ctx,
                                              int id,
                                              String first_name,
                                              String last_name,
                                              String email,
                                              String phone,
                                              Department department,
                                              String username,
                                              String password,
                                              String error) {

        Map<String, Object> model = new HashMap<>();
        model.put("prevId", id);
        model.put("prevFirstName", first_name);
        model.put("prevLastName", last_name);
        model.put("prevEmail", email);
        model.put("prevPhone", phone);
        model.put("prevDepartment", department);
        model.put("prevUsername", username);
        model.put("prevPassword", password);

        model.put("error", error);

        model.put("DepartmentOption", Department.values());

        ctx.render("add_user.vm", model);
    }


    public static void handleAddUserRequest(Context ctx) {
        int id = ctx.formParamAsClass("id", Integer.class).getOrDefault(-1);
        String first_name = ctx.formParam("first_name");
        String last_name = ctx.formParam("last_name");
        String email = ctx.formParam("email");
        String phone = ctx.formParam("phone");
        Department department = ctx.formParamAsClass("department", Department.class).get();
        String username = ctx.formParam("username");
        String password = ctx.formParam("password");


        try {
            UsersDTO user = new UsersDTO(id, first_name, last_name, email, phone, department, username, password);

                UsersDAO.insertNewUser(user);

            ctx.redirect("/users");
        } catch (Exception e) {
            renderAddEditUserForm(ctx, id, first_name, last_name, email, phone, department, username, password, "Username already exist!");
        }
    }


}
