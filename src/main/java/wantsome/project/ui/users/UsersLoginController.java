package wantsome.project.ui.users;


import io.javalin.http.Context;
import wantsome.project.db.users.dao.UsersDAO;
import wantsome.project.db.users.dto.UsersDTO;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class UsersLoginController {

    public static void checkUserIsLoggedIn(Context ctx) {
        if (isRestrictedPage(ctx.path())
                && ctx.sessionAttribute("currentUser") == null) {
            ctx.redirect("/login");
        }
    }

    private static boolean isRestrictedPage(String path) {
        return path.startsWith("/generators")
                || path.startsWith("/generators/inProgressGenerators");
    }

    public static void showLoginPage(Context ctx) {
        renderLoginPage(ctx, null, null);
    }

    public static void handleLoginRequest(Context ctx) {
        String username = ctx.formParam("username");
        String password = ctx.formParam("password");

        Optional<UsersDTO> user = UsersDAO.loadUserDetails(username);

        if (user.isPresent() && user.get().getPassword().equals(password)) {
            ctx.sessionAttribute("currentUser", user.get());
            ctx.redirect("/generators");
        } else {
            renderLoginPage(ctx, username, "Invalid username or password");
        }
    }

    public static void handleLogoutRequest(Context ctx) {
        ctx.sessionAttribute("currentUser", null);
        ctx.redirect("/login");
    }


    private static void renderLoginPage(Context ctx, String username, String error) {
        Map<String, Object> model = new HashMap<>();
        model.put("username", username);
        model.put("error", error);
        ctx.render("login.vm", model);
    }

    public static void addUserInfoToModel(Context ctx, Map<String, Object> model) {
        model.put("currentUser", ctx.sessionAttribute("currentUser"));
    }
}
