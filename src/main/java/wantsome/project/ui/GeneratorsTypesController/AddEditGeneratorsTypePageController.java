package wantsome.project.ui.GeneratorsTypesController;

import io.javalin.core.validation.JavalinValidation;
import io.javalin.http.Context;
import wantsome.project.db.generators_types.dao.GeneratorTypeDAO;
import wantsome.project.db.generators_types.dto.Category;
import wantsome.project.db.generators_types.dto.GeneratorsTypesDTO;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class AddEditGeneratorsTypePageController {
    static {
        JavalinValidation.register(Category.class, s -> s != null && !s.isEmpty() ? Category.valueOf(s) : null);
    }

    public static void showAddGeneratorsTypePage(Context ctx) {
        renderAddUpdateForm(ctx, -1, "", "", Category.LOW_COST, null);
    }

    public static void showUpdateCategoryPage(Context ctx) {
        int id = ctx.pathParamAsClass("id", Integer.class).get();
        Optional<GeneratorsTypesDTO> optionalGeneratorsTypes = GeneratorTypeDAO.load(id);
        GeneratorsTypesDTO generatorsTypes = optionalGeneratorsTypes.orElseThrow(() -> new RuntimeException("Generator type with id " + id + " not found!"));

        renderAddUpdateForm(ctx, generatorsTypes.getId(),
                generatorsTypes.getProduct_type(),
                generatorsTypes.getCapacity(),
                generatorsTypes.getCategory(), null);
    }

    private static void renderAddUpdateForm(Context ctx, int id,
                                            String product_type,
                                            String capacity,
                                            Category category,
                                            String error) {
        Map<String, Object> model = new HashMap<>();

        model.put("prevId", id);
        model.put("prevProductType", product_type);
        model.put("prevCapacity", capacity);
        model.put("preCategory", category);
        model.put("error", error);

        model.put("CategoryOption", Category.values());


        ctx.render("add_edit_generators_types.vm", model);
    }

    public static void handleAddUpdateRequest(Context ctx) {
        int id = ctx.formParamAsClass("id", Integer.class).getOrDefault(-1);
        String product_type = ctx.formParam("product_type");
        String capacity = ctx.formParam("capacity");
        Category category = ctx.formParamAsClass("category", Category.class).get();

        try {
            GeneratorsTypesDTO generatorsTypes = new GeneratorsTypesDTO(id, product_type, capacity, category);

            if (id > 0) {
                GeneratorTypeDAO.updateGeneratorType(generatorsTypes);
            } else {
                GeneratorTypeDAO.insertGeneratorType(generatorsTypes);
            }
            ctx.redirect("/generators_types");
        } catch (Exception e) {
            renderAddUpdateForm(ctx, id, product_type, capacity, category, e.getMessage());
        }
    }
}
