package wantsome.project.ui.GeneratorsTypesController;

import io.javalin.http.Context;
import wantsome.project.db.generators_types.dao.GeneratorTypeDAO;
import wantsome.project.db.generators_types.dto.Category;

import java.util.HashMap;
import java.util.Map;

public class GeneratorsTypesPageController {

    public static void showGeneratorsTypesPage(Context ctx) {
        renderGeneratorsTypesPage(ctx, null);


    }

    private static void renderGeneratorsTypesPage(Context ctx, String error) {
        Map<String, Object> model = new HashMap<>();
        model.put("generators_types", GeneratorTypeDAO.loadAllGeneratorsTypes());

        model.put("error", error);

        model.put("CategoryOption", Category.values());

        ctx.render("generators_types.vm", model);
    }

    public static void handleDeleteGeneratorsTypesRequest(Context ctx) {

        try {
            int id = ctx.pathParamAsClass("id", Integer.class).get();
            GeneratorTypeDAO.deleteGeneratorType(id);
            ctx.redirect("/generators_types");
        } catch (Exception e) {
            renderGeneratorsTypesPage(ctx, "Generator can't be deleted, generator type in use!");

        }
    }
}
