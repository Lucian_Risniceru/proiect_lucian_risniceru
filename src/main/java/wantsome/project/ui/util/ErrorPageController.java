package wantsome.project.ui.util;


import io.javalin.http.Context;

import java.util.HashMap;
import java.util.Map;

public class ErrorPageController {
    public static void handleException(Exception exception, Context ctx) {
        Map<String, Object> model = new HashMap<>();
        model.put("error", exception.getMessage());
        ctx.render("error.vm", model);
    }
}
