package wantsome.project;

import io.javalin.Javalin;
import io.javalin.http.staticfiles.Location;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wantsome.project.db.DbInitService;
import wantsome.project.ui.Generators.GeneratorPageController;
import wantsome.project.ui.Generators.ProductionAddEditGeneratorController;
import wantsome.project.ui.Generators.ProductionPageController;
import wantsome.project.ui.Generators.UpdateStatusSellOrReserveController;
import wantsome.project.ui.GeneratorsTypesController.AddEditGeneratorsTypePageController;
import wantsome.project.ui.GeneratorsTypesController.GeneratorsTypesPageController;
import wantsome.project.ui.Reports.ReportsPageController;
import wantsome.project.ui.users.*;
import wantsome.project.ui.util.ErrorPageController;

public class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        setup();
        startWebServer();
    }

    private static void setup() {

        DbInitService.initDatabase();

    }

    private static void startWebServer() {
        Javalin app = Javalin.create(config -> {
            config.addStaticFiles("/public", Location.CLASSPATH);
            config.enableDevLogging();
        }).start();


        app.get("/", ctx -> ctx.redirect("/generators"));
        app.exception(Exception.class, (e, ctx) -> {
            logger.error("Unexpected exception", e);
            ctx.html("Unexpected exception: " + e);
        });

        app.before(UsersLoginController::checkUserIsLoggedIn);
        app.get("/login", UsersLoginController::showLoginPage);
        app.post("/login", UsersLoginController::handleLoginRequest);
        app.get("/logout", UsersLoginController::handleLogoutRequest);


        app.get("/generators", GeneratorPageController::showGeneratorsPage);
        app.get("/generators/delete/{id}", GeneratorPageController::handleDeleteGeneratorRequest);

        app.get("/generators/sell_or_reserve/{id}", UpdateStatusSellOrReserveController::showSellOrReserveForm);
        app.post("/generators/sell_or_reserve/{id}", UpdateStatusSellOrReserveController::handleUpdateSellOrReservePostRequest);

        app.get("/generators/inProgressGenerators", ProductionPageController::showInProgressGenerators);
        app.get("/generators/inProgressGenerators/delete/{id}", ProductionPageController::handleDeleteGeneratorRequest);

        app.get("/generators/add_generator", ProductionAddEditGeneratorController::showAddForm);
        app.post("/generators/add_generator", ProductionAddEditGeneratorController::handleAddRequest);

        app.get("/generators_types", GeneratorsTypesPageController::showGeneratorsTypesPage);
        app.get("/generators_types/delete/{id}", GeneratorsTypesPageController::handleDeleteGeneratorsTypesRequest);

        app.get("/generators_types/add_generators_types", AddEditGeneratorsTypePageController::showAddGeneratorsTypePage);
        app.post("/generators_types/add_generators_types", AddEditGeneratorsTypePageController::handleAddUpdateRequest);

        app.get("/generators_types/update_generators_types/{id}", AddEditGeneratorsTypePageController::showUpdateCategoryPage);
        app.post("/generators_types/update_generators_types/{id}", AddEditGeneratorsTypePageController::handleAddUpdateRequest);

        app.get("/generators/inProgressGenerators/{id}", ProductionPageController::showUpdateStatusInProgressForm);
        app.post("/generators/inProgressGenerators/{id}", ProductionPageController::handleUpdateInProgressPriorityRequest);

        app.get("/generators/reports", ReportsPageController::showShippedGenerators);

        app.get("/update_user_details/{id}", UpdateCurrentUserDetailsController::showUpdateUserDetailsPage);
        app.post("/update_user_details/{id}", UpdateCurrentUserDetailsController::handleUpdateRequest);

        app.get("/users", UsersPageController::showUsersPage);
        app.get("/users/delete/{id}", UsersPageController::handleDeleteUserRequest);

        app.get("/users/edit_user/{id}", EditUserController::showEditUserDetailsPage);
        app.post("/users/edit_user", EditUserController::handleEditUserRequest);

        app.get("/users/add_user", AddUserController::showAddUserPage);
        app.post("/users/add_user", AddUserController::handleAddUserRequest);


        app.exception(Exception.class, ErrorPageController::handleException);
        logger.info("Server started: http://localhost:" + app.port());
    }
}
