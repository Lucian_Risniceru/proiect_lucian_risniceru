package wantsome.project.db.generators.dto;

public enum ProductionPriority {
    HIGH("High"),
    MEDIUM("Medium"),
    LOW("Low");

    private final String label;

    ProductionPriority(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

}

