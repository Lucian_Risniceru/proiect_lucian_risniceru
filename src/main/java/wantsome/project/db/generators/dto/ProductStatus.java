package wantsome.project.db.generators.dto;

public enum ProductStatus {
    IN_PROGRESS("In progress"),
    STOCK("Stock"),
    RESERVED("Reserved"),
    READY_FOR_SHIPMENT("Ready for Shipment"),
    SHIPPED("Shipped");

    private final String label;

    ProductStatus(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

}
