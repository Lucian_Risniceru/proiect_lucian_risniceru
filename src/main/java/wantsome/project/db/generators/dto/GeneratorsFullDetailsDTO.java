package wantsome.project.db.generators.dto;

import wantsome.project.db.generators_types.dto.Category;

import java.sql.Date;
import java.util.Objects;

public class GeneratorsFullDetailsDTO {

    private final int id;
    private final Date manufacturing_date;
    private final String product_serial_number;
    private final String product_type;
    private final String capacity;
    private final Category category;
    private final String first_name;
    private final String last_name;
    private final String customer;
    private final String remark;
    private final ProductStatus product_status;
    private final Date reservation_date;
    private final Date shipping_date;
    private final ProductionPriority production_priority;


    public GeneratorsFullDetailsDTO(int id, Date manufacturing_date
            , String product_serial_number, String product_type
            , String capacity, Category category, String first_name
            , String last_name, String customer, String remark
            , ProductStatus product_status, Date reservation_date
            , Date shipping_date, ProductionPriority production_priority) {

        this.id = id;
        this.manufacturing_date = manufacturing_date;
        this.product_serial_number = product_serial_number;
        this.product_type = product_type;
        this.capacity = capacity;
        this.category = category;
        this.first_name = first_name;
        this.last_name = last_name;
        this.customer = customer;
        this.remark = remark;
        this.product_status = product_status;
        this.reservation_date = reservation_date;
        this.shipping_date = shipping_date;
        this.production_priority = production_priority;
    }

    public int getId() {
        return id;
    }

    public Date getManufacturing_date() {
        return manufacturing_date;
    }

    public String getProduct_serial_number() {
        return product_serial_number;
    }

    public String getProduct_type() {
        return product_type;
    }

    public String getCapacity() {
        return capacity;
    }

    public Category getCategory() {
        return category;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getCustomer() {
        return customer;
    }

    public String getRemark() {
        return remark;
    }

    public ProductStatus getProduct_status() {
        return product_status;
    }

    public Date getReservation_date() {
        return reservation_date;
    }

    public Date getShipping_date() {
        return shipping_date;
    }

    public ProductionPriority getProduction_priority() {
        return production_priority;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GeneratorsFullDetailsDTO)) return false;
        GeneratorsFullDetailsDTO that = (GeneratorsFullDetailsDTO) o;
        return getId() == that.getId() && Objects.equals(getManufacturing_date(), that.getManufacturing_date()) && Objects.equals(getProduct_serial_number(), that.getProduct_serial_number()) && Objects.equals(getProduct_type(), that.getProduct_type()) && Objects.equals(getCapacity(), that.getCapacity()) && getCategory() == that.getCategory() && Objects.equals(getFirst_name(), that.getFirst_name()) && Objects.equals(getLast_name(), that.getLast_name()) && Objects.equals(getCustomer(), that.getCustomer()) && Objects.equals(getRemark(), that.getRemark()) && getProduct_status() == that.getProduct_status() && Objects.equals(getReservation_date(), that.getReservation_date()) && Objects.equals(getShipping_date(), that.getShipping_date()) && getProduction_priority() == that.getProduction_priority();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getManufacturing_date(), getProduct_serial_number(), getProduct_type(), getCapacity(), getCategory(), getFirst_name(), getLast_name(), getCustomer(), getRemark(), getProduct_status(), getReservation_date(), getShipping_date(), getProduction_priority());
    }

    @Override
    public String toString() {
        return "GeneratorsFullDetailsDTO{" +
                "id=" + id +
                ", manufacturing_date=" + manufacturing_date +
                ", product_serial_number='" + product_serial_number + '\'' +
                ", product_type='" + product_type + '\'' +
                ", capacity='" + capacity + '\'' +
                ", category=" + category +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", customer='" + customer + '\'' +
                ", remark='" + remark + '\'' +
                ", product_status=" + product_status +
                ", reservation_date=" + reservation_date +
                ", shipping_date=" + shipping_date +
                ", production_priority=" + production_priority +
                '}';
    }
}
