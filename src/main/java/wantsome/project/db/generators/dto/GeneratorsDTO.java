package wantsome.project.db.generators.dto;


import java.sql.Date;
import java.util.Objects;

public class GeneratorsDTO {
    private final int id;
    private final String product_serial_number;
    private final int generator_type_id;
    private final Date manufacturing_date;
    private final int user_id;
    private final String customer;
    private final String remark;
    private final ProductStatus product_status;
    private final Date reservation_date;
    private final Date shipping_date;
    private final ProductionPriority production_priority;


    public GeneratorsDTO(int id, String product_serial_number
            , int generator_type_id, Date manufacturing_date
            , int user_id, String customer, String remark
            , ProductStatus product_status, Date reservation_date
            , Date shipping_date, ProductionPriority production_priority) {

        this.id = id;
        this.product_serial_number = product_serial_number;
        this.generator_type_id = generator_type_id;
        this.manufacturing_date = manufacturing_date;
        this.user_id = user_id;
        this.customer = customer;
        this.remark = remark;
        this.product_status = product_status;
        this.reservation_date = reservation_date;
        this.shipping_date = shipping_date;
        this.production_priority = production_priority;
    }

    public int getId() {
        return id;
    }

    public String getProduct_serial_number() {
        return product_serial_number;
    }

    public int getGenerator_type_id() {
        return generator_type_id;
    }

    public Date getManufacturing_date() {
        return manufacturing_date;
    }

    public int getUser_id() {
        return user_id;
    }

    public String getCustomer() {
        return customer;
    }

    public String getRemark() {
        return remark;
    }

    public ProductStatus getProduct_status() {
        return product_status;
    }

    public Date getReservation_date() {
        return reservation_date;
    }

    public Date getShipping_date() {
        return shipping_date;
    }

    public ProductionPriority getProduction_priority() {
        return production_priority;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GeneratorsDTO)) return false;
        GeneratorsDTO that = (GeneratorsDTO) o;
        return getId() == that.getId() && getUser_id() == that.getUser_id()
                && getProduct_serial_number().equals(that.getProduct_serial_number())
                && getGenerator_type_id() == (that.getGenerator_type_id())
                && getManufacturing_date().equals(that.getManufacturing_date())
                && Objects.equals(getCustomer(), that.getCustomer())
                && Objects.equals(getRemark(), that.getRemark())
                && getProduct_status() == that.getProduct_status()
                && Objects.equals(getReservation_date(), that.getReservation_date())
                && Objects.equals(getShipping_date(), that.getShipping_date())
                && getProduction_priority() == that.getProduction_priority();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getProduct_serial_number()
                , getGenerator_type_id(), getManufacturing_date()
                , getUser_id(), getCustomer(), getRemark()
                , getProduct_status(), getReservation_date()
                , getShipping_date(), getProduction_priority());
    }

    @Override
    public String toString() {
        return "GeneratorsDTO{" +
                "id=" + id +
                ", product_serial_number='" + product_serial_number + '\'' +
                ", generator_type='" + generator_type_id + '\'' +
                ", manufacturing_date=" + manufacturing_date +
                ", user_id=" + user_id +
                ", customer='" + customer + '\'' +
                ", remark='" + remark + '\'' +
                ", product_status=" + product_status +
                ", reservation_date=" + reservation_date +
                ", delivery_date=" + shipping_date +
                ", productionPriority=" + production_priority +
                '}';
    }
}
