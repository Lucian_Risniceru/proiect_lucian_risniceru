package wantsome.project.db.generators.dao;


import wantsome.project.db.DbManager;
import wantsome.project.db.generators.dto.GeneratorsDTO;
import wantsome.project.db.generators.dto.GeneratorsFullDetailsDTO;
import wantsome.project.db.generators.dto.ProductStatus;
import wantsome.project.db.generators.dto.ProductionPriority;
import wantsome.project.db.generators_types.dto.Category;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class GeneratorsDAO {
    public static List<GeneratorsFullDetailsDTO> loadFullDetailsGenerators() {
        String SQL = "SELECT " +
                "GENERATORS.ID," +
                "GENERATORS.MANUFACTURING_DATE ," +
                "GENERATORS.PRODUCT_SERIAL_NUMBER ," +
                "GENERATORS_TYPES.PRODUCT_TYPE ," +
                "GENERATORS_TYPES.CAPACITY ," +
                "GENERATORS_TYPES.CATEGORY ," +
                "USERS.FIRST_NAME ," +
                "USERS.LAST_NAME ," +
                "GENERATORS.CUSTOMER ," +
                "GENERATORS.REMARK ," +
                "GENERATORS.PRODUCT_STATUS ," +
                "GENERATORS.RESERVATION_DATE ," +
                "GENERATORS.SHIPPING_DATE," +
                "GENERATORS.PRODUCTION_PRIORITY " +
                "FROM GENERATORS " +
                "JOIN GENERATORS_TYPES ON GENERATORS.GENERATOR_TYPE_ID = GENERATORS_TYPES.ID " +
                "JOIN USERS ON GENERATORS.USER_ID = USERS.ID " +
                "ORDER BY GENERATORS.MANUFACTURING_DATE ;";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(SQL);
             ResultSet rs = ps.executeQuery()) {

            List<GeneratorsFullDetailsDTO> generators = new ArrayList<>();
            while (rs.next()) {
                generators.add(new GeneratorsFullDetailsDTO(
                        rs.getInt("ID"),
                        rs.getDate("MANUFACTURING_DATE"),
                        rs.getString("PRODUCT_SERIAL_NUMBER"),
                        rs.getString("PRODUCT_TYPE"),
                        rs.getString("CAPACITY"),
                        Category.valueOf(rs.getString("CATEGORY")),
                        rs.getString("FIRST_NAME"),
                        rs.getString("LAST_NAME"),
                        rs.getString("CUSTOMER"),
                        rs.getString("REMARK"),
                        ProductStatus.valueOf(rs.getString("PRODUCT_STATUS")),
                        rs.getDate("RESERVATION_DATE"),
                        rs.getDate("SHIPPING_DATE"),
                        ProductionPriority.valueOf(rs.getString("PRODUCTION_PRIORITY"))));
            }
            return generators;

        } catch (SQLException e) {
            throw new RuntimeException("Error loading generators: " + e.getMessage());
        }

    }


    public static List<GeneratorsFullDetailsDTO> loadInProgressGenerators() {
        String SQL = "SELECT \n" +
                "GENERATORS.ID,\n" +
                "GENERATORS.MANUFACTURING_DATE ,\n" +
                "GENERATORS.PRODUCT_SERIAL_NUMBER ,\n" +
                "GENERATORS_TYPES.PRODUCT_TYPE ,\n" +
                "GENERATORS_TYPES.CAPACITY ,\n" +
                "GENERATORS_TYPES.CATEGORY ,\n" +
                "USERS.FIRST_NAME ,\n" +
                "USERS.LAST_NAME ,\n" +
                "GENERATORS.CUSTOMER ,\n" +
                "GENERATORS.REMARK ,\n" +
                "GENERATORS.PRODUCT_STATUS ,\n" +
                "GENERATORS.RESERVATION_DATE ,\n" +
                "GENERATORS.SHIPPING_DATE,\n" +
                "GENERATORS.PRODUCTION_PRIORITY \n" +
                "FROM GENERATORS\n" +
                "JOIN GENERATORS_TYPES ON GENERATORS.GENERATOR_TYPE_ID = GENERATORS_TYPES.ID\n" +
                "JOIN USERS ON GENERATORS.USER_ID = USERS.ID\n" +
                "WHERE GENERATORS.PRODUCT_STATUS ='IN_PROGRESS'\n" +
                "ORDER BY GENERATORS.MANUFACTURING_DATE;\n";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(SQL);
             ResultSet rs = ps.executeQuery()) {

            List<GeneratorsFullDetailsDTO> generators = new ArrayList<>();
            while (rs.next()) {
                generators.add(new GeneratorsFullDetailsDTO(
                        rs.getInt("ID"),
                        rs.getDate("MANUFACTURING_DATE"),
                        rs.getString("PRODUCT_SERIAL_NUMBER"),
                        rs.getString("PRODUCT_TYPE"),
                        rs.getString("CAPACITY"),
                        Category.valueOf(rs.getString("CATEGORY")),
                        rs.getString("FIRST_NAME"),
                        rs.getString("LAST_NAME"),
                        rs.getString("CUSTOMER"),
                        rs.getString("REMARK"),
                        ProductStatus.valueOf(rs.getString("PRODUCT_STATUS")),
                        rs.getDate("RESERVATION_DATE"),
                        rs.getDate("SHIPPING_DATE"),
                        ProductionPriority.valueOf(rs.getString("PRODUCTION_PRIORITY"))));
            }
            return generators;

        } catch (SQLException e) {
            throw new RuntimeException("Error loading generators: " + e.getMessage());
        }

    }

    public static List<GeneratorsFullDetailsDTO> shippedGenerators() {
        String SQL = "SELECT \n" +
                "GENERATORS.ID,\n" +
                "GENERATORS.MANUFACTURING_DATE ,\n" +
                "GENERATORS.PRODUCT_SERIAL_NUMBER ,\n" +
                "GENERATORS_TYPES.PRODUCT_TYPE ,\n" +
                "GENERATORS_TYPES.CAPACITY ,\n" +
                "GENERATORS_TYPES.CATEGORY ,\n" +
                "USERS.FIRST_NAME ,\n" +
                "USERS.LAST_NAME ,\n" +
                "GENERATORS.CUSTOMER ,\n" +
                "GENERATORS.REMARK ,\n" +
                "GENERATORS.PRODUCT_STATUS ,\n" +
                "GENERATORS.RESERVATION_DATE ,\n" +
                "GENERATORS.SHIPPING_DATE,\n" +
                "GENERATORS.PRODUCTION_PRIORITY \n" +
                "FROM GENERATORS\n" +
                "JOIN GENERATORS_TYPES ON GENERATORS.GENERATOR_TYPE_ID = GENERATORS_TYPES.ID\n" +
                "JOIN USERS ON GENERATORS.USER_ID = USERS.ID\n" +
                "WHERE GENERATORS.PRODUCT_STATUS ='SHIPPED'\n" +
                "ORDER BY GENERATORS.MANUFACTURING_DATE;\n";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(SQL);
             ResultSet rs = ps.executeQuery()) {

            List<GeneratorsFullDetailsDTO> generators = new ArrayList<>();
            while (rs.next()) {
                generators.add(new GeneratorsFullDetailsDTO(
                        rs.getInt("ID"),
                        rs.getDate("MANUFACTURING_DATE"),
                        rs.getString("PRODUCT_SERIAL_NUMBER"),
                        rs.getString("PRODUCT_TYPE"),
                        rs.getString("CAPACITY"),
                        Category.valueOf(rs.getString("CATEGORY")),
                        rs.getString("FIRST_NAME"),
                        rs.getString("LAST_NAME"),
                        rs.getString("CUSTOMER"),
                        rs.getString("REMARK"),
                        ProductStatus.valueOf(rs.getString("PRODUCT_STATUS")),
                        rs.getDate("RESERVATION_DATE"),
                        rs.getDate("SHIPPING_DATE"),
                        ProductionPriority.valueOf(rs.getString("PRODUCTION_PRIORITY"))));
            }
            return generators;

        } catch (SQLException e) {
            throw new RuntimeException("Error loading generators: " + e.getMessage());
        }

    }

    public static Optional<GeneratorsFullDetailsDTO> load(int id) {
        String SQL = "SELECT " +
                "GENERATORS.ID," +
                "GENERATORS.MANUFACTURING_DATE ," +
                "GENERATORS.PRODUCT_SERIAL_NUMBER ," +
                "GENERATORS_TYPES.PRODUCT_TYPE ," +
                "GENERATORS_TYPES.CAPACITY ," +
                "GENERATORS_TYPES.CATEGORY ," +
                "USERS.FIRST_NAME ," +
                "USERS.LAST_NAME ," +
                "GENERATORS.CUSTOMER ," +
                "GENERATORS.REMARK ," +
                "GENERATORS.PRODUCT_STATUS ," +
                "GENERATORS.RESERVATION_DATE ," +
                "GENERATORS.SHIPPING_DATE," +
                "GENERATORS.PRODUCTION_PRIORITY " +
                "FROM GENERATORS " +
                "JOIN GENERATORS_TYPES ON GENERATORS.GENERATOR_TYPE_ID = GENERATORS_TYPES.ID " +
                "JOIN USERS ON GENERATORS.USER_ID = USERS.ID " +
                "WHERE GENERATORS.ID= ?";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(SQL)) {

            ps.setInt(1, id);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return Optional.of(
                            new GeneratorsFullDetailsDTO(
                                    rs.getInt("id"),
                                    rs.getDate("MANUFACTURING_DATE"),
                                    rs.getString("PRODUCT_SERIAL_NUMBER"),
                                    rs.getString("PRODUCT_TYPE"),
                                    rs.getString("CAPACITY"),
                                    Category.valueOf(rs.getString("CATEGORY")),
                                    rs.getString("FIRST_NAME"),
                                    rs.getString("LAST_NAME"),
                                    rs.getString("CUSTOMER"),
                                    rs.getString("REMARK"),
                                    ProductStatus.valueOf(rs.getString("PRODUCT_STATUS")),
                                    rs.getDate("RESERVATION_DATE"),
                                    rs.getDate("SHIPPING_DATE"),
                                    ProductionPriority.valueOf(rs.getString("PRODUCTION_PRIORITY"))));
                }
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error loading generators: " + e.getMessage());
        }
    }


    public static void insertGenerator(GeneratorsDTO generator) {

        String SQL = "INSERT INTO GENERATORS (PRODUCT_SERIAL_NUMBER, GENERATOR_TYPE_ID, MANUFACTURING_DATE," +
                "USER_ID,CUSTOMER,REMARK,PRODUCT_STATUS,RESERVATION_DATE," +
                "SHIPPING_DATE,PRODUCTION_PRIORITY) " +
                "VALUES(?,?,?,?,?,?,?,?,?,?);";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(SQL)) {


            ps.setString(1, generator.getProduct_serial_number());
            ps.setInt(2, generator.getGenerator_type_id());
            ps.setDate(3, generator.getManufacturing_date());
            ps.setInt(4, generator.getUser_id());
            ps.setString(5, generator.getCustomer());
            ps.setString(6, generator.getRemark());
            ps.setString(7, generator.getProduct_status().name());
            ps.setDate(8, generator.getReservation_date());
            ps.setDate(9, generator.getShipping_date());
            ps.setString(10, generator.getProduction_priority().name());

            ps.execute();

        } catch (SQLException e) {
            String details = e.getMessage().contains("UNIQUE constraint failed: PRODUCT_SERIAL_NUMBER") ?
                    "a generator with same PRODUCT_SERIAL_NUMBER  already exists" :
                    e.getMessage();
            throw new RuntimeException("Error saving new generator: " + details);
        }
    }

    public static void updateGenerator(GeneratorsDTO generator) {

        String SQL = "UPDATE GENERATORS " +
                "SET PRODUCT_SERIAL_NUMBER = ?," +
                "GENERATOR_TYPE_ID = ?," +
                "MANUFACTURING_DATE = ?," +
                "USER_ID = ?," +
                "CUSTOMER = ?," +
                "REMARK = ?," +
                "PRODUCT_STATUS = ?," +
                "RESERVATION_DATE = ?," +
                "SHIPPING_DATE = ?," +
                "PRODUCTION_PRIORITY = ? " +
                "WHERE ID = ?;";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(SQL)) {

            ps.setString(1, generator.getProduct_serial_number());
            ps.setInt(2, generator.getGenerator_type_id());
            ps.setDate(3, generator.getManufacturing_date());
            ps.setInt(4, generator.getUser_id());
            ps.setString(5, generator.getCustomer());
            ps.setString(6, generator.getRemark());
            ps.setString(7, generator.getProduct_status().name());
            ps.setDate(8, generator.getReservation_date());
            ps.setDate(9, generator.getShipping_date());
            ps.setString(10, generator.getProduction_priority().name());
            ps.setInt(11, generator.getId());

            ps.execute();

        } catch (SQLException e) {
            String details = e.getMessage().contains("UNIQUE constraint failed: PRODUCT_SERIAL_NUMBER") ?
                    "a generator with same PRODUCT_SERIAL_NUMBER  already exists" :
                    e.getMessage();
            throw new RuntimeException("Error saving new generator: " + details);
        }
    }

    public static void updateSellOrReserveGenerator(GeneratorsDTO generator) {

        String SQL = "UPDATE GENERATORS \n" +
                "SET \n" +
                "USER_ID = ?,\n" +
                "CUSTOMER = ?,\n" +
                "REMARK= ?,\n" +
                "PRODUCT_STATUS  = ?,\n" +
                "RESERVATION_DATE = ? ,\n" +
                "SHIPPING_DATE = ?\n" +
                "WHERE ID= ?;";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(SQL)) {

            ps.setInt(1, generator.getUser_id());
            ps.setString(2, generator.getCustomer());
            ps.setString(3, generator.getRemark());
            ps.setString(4, generator.getProduct_status().name());
            ps.setDate(5, generator.getReservation_date());
            ps.setDate(6, generator.getShipping_date());
            ps.setInt(7, generator.getId());

            ps.execute();

        } catch (SQLException e) {

            throw new RuntimeException("Error saving new generator: ");
        }
    }

    public static void updateStatusProductionPriority(GeneratorsFullDetailsDTO generator) {

        String SQL = "UPDATE GENERATORS \n" +
                "SET REMARK= ?,\n" +
                "PRODUCT_STATUS  =?,\n" +
                "PRODUCTION_PRIORITY =?\n" +
                "WHERE ID= ?;";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(SQL)) {

            ps.setString(1, generator.getRemark());
            ps.setString(2, generator.getProduct_status().name());
            ps.setString(3, generator.getProduction_priority().name());
            ps.setInt(4, generator.getId());

            ps.execute();
        } catch (SQLException e) {
            throw new RuntimeException("Error updating status : ");
        }

    }

    public static void deleteGenerator(int id) {

        String SQL = "DELETE FROM GENERATORS  WHERE ID=?;";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(SQL)) {

            ps.setInt(1, id);

            ps.execute();

        } catch (SQLException e) {
            throw new RuntimeException("Error deleting generator " + id + ": " + e.getMessage());
        }
    }
}
