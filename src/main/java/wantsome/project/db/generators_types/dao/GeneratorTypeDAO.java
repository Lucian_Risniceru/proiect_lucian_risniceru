package wantsome.project.db.generators_types.dao;

import wantsome.project.db.DbManager;
import wantsome.project.db.generators_types.dto.Category;
import wantsome.project.db.generators_types.dto.GeneratorsTypesDTO;
import wantsome.project.db.generators_types.dto.GeneratorsTypesStockDTO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class GeneratorTypeDAO {
    public static List<GeneratorsTypesDTO> loadAllGeneratorsTypes() {

        String SQL = "SELECT * FROM GENERATORS_TYPES";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(SQL);
             ResultSet rs = ps.executeQuery()) {

            List<GeneratorsTypesDTO> results = new ArrayList<>();
            while (rs.next()) {
                results.add(new GeneratorsTypesDTO(
                        rs.getInt("ID"),
                        rs.getString("PRODUCT_TYPE"),
                        rs.getString("CAPACITY"),
                        Category.valueOf(rs.getString("CATEGORY"))));
            }
            return results;

        } catch (SQLException e) {
            throw new RuntimeException("Error loading generators: " + e.getMessage());
        }
    }

    public static Optional<GeneratorsTypesDTO> load(int id) {

        String SQL = "SELECT * FROM GENERATORS_TYPES WHERE ID=? ";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(SQL)) {

            ps.setInt(1, id);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    GeneratorsTypesDTO generatorsTypes = extractGeneratorTypesFromResult(rs);
                    return Optional.of(generatorsTypes);
                }
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error laoding generator type " + id + ":" + e.getMessage());
        }

    }

    private static GeneratorsTypesDTO extractGeneratorTypesFromResult(ResultSet rs) throws SQLException {
        return new GeneratorsTypesDTO(
                rs.getInt("ID"),
                rs.getString("PRODUCT_TYPE"),
                rs.getString("CAPACITY"),
                Category.valueOf(rs.getString("CATEGORY")));
    }


    public static void insertGeneratorType(GeneratorsTypesDTO generator) {

        String SQL = "INSERT INTO GENERATORS_TYPES (PRODUCT_TYPE, CAPACITY, CATEGORY) VALUES(?,?,?);";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(SQL)) {


            ps.setString(1, generator.getProduct_type());
            ps.setString(2, generator.getCapacity());
            ps.setString(3, generator.getCategory().name());

            ps.execute();


        } catch (SQLException e) {
            String details = e.getMessage().contains("UNIQUE constraint failed: PRODUCT_TYPE") ?
                    "a product with same PRODUCT_TYPE already exists" :
                    e.getMessage();
            throw new RuntimeException("Error saving new generator: " + details);
        }
    }

    public static void updateGeneratorType(GeneratorsTypesDTO generatorType) {

        String SQL = "UPDATE GENERATORS_TYPES " +
                "SET PRODUCT_TYPE = ?, CAPACITY = ?, CATEGORY =? " +
                "WHERE ID  = ?;";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(SQL)) {


            ps.setString(1, generatorType.getProduct_type());
            ps.setString(2, generatorType.getCapacity());
            ps.setString(3, generatorType.getCategory().name());
            ps.setInt(4, generatorType.getId());

            ps.execute();

        } catch (SQLException e) {
            String details = e.getMessage().contains("UNIQUE constraint failed: PRODUCT_TYPE") ?
                    "a product with same name already exists" :
                    e.getMessage();
            throw new RuntimeException("Error updating Generators_Types: " + details);
        }
    }

    public static List<GeneratorsTypesStockDTO> outOfStock() {
        String SQL = "SELECT GENERATORS_TYPES.PRODUCT_TYPE,\n" +
                "GENERATORS_TYPES.CATEGORY  ,\n" +
                "(SELECT COUNT(*) FROM GENERATORS\n" +
                "WHERE GENERATORS.PRODUCT_STATUS ='STOCK'\n" +
                "AND GENERATORS_TYPES.ID = GENERATORS.GENERATOR_TYPE_ID )\n" +
                "AS 'STOCK'\n" +
                "FROM GENERATORS_TYPES\n" +
                "WHERE (SELECT COUNT(*) FROM GENERATORS \n" +
                "WHERE GENERATORS.PRODUCT_STATUS ='STOCK' \n" +
                "AND GENERATORS_TYPES.ID = GENERATORS.GENERATOR_TYPE_ID ) =0;";
        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(SQL);
             ResultSet rs = ps.executeQuery()) {

            List<GeneratorsTypesStockDTO> generators_types = new ArrayList<>();
            while (rs.next()) {
                generators_types.add(new GeneratorsTypesStockDTO(
                        rs.getString("PRODUCT_TYPE"),
                        Category.valueOf(rs.getString("CATEGORY")),
                        rs.getInt("STOCK")));
            }
            return generators_types;
        } catch (SQLException e) {
            throw new RuntimeException("Error loading generators: " + e.getMessage());
        }
    }


    public static void deleteGeneratorType(int id) {

        String SQL = "DELETE FROM GENERATORS_TYPES WHERE ID = ?;";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(SQL)) {

            ps.setInt(1, id);

            ps.execute();

        } catch (SQLException e) {
            throw new RuntimeException("Error deleting generator type " + id + ": " + e.getMessage());
        }
    }
}
