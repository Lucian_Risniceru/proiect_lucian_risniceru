package wantsome.project.db.generators_types.dto;

import java.util.Objects;

public class GeneratorsTypesDTO {
    private final int id;
    private final String product_type;
    private final String capacity;
    private final Category category;

    public GeneratorsTypesDTO(int id, String product_type, String capacity, Category category) {
        this.id = id;
        this.product_type = product_type;
        this.capacity = capacity;
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public String getProduct_type() {
        return product_type;
    }

    public String getCapacity() {
        return capacity;
    }

    public Category getCategory() {
        return category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GeneratorsTypesDTO)) return false;
        GeneratorsTypesDTO that = (GeneratorsTypesDTO) o;
        return getId() == that.getId()
                && getProduct_type().equals(that.getProduct_type())
                && getCapacity().equals(that.getCapacity())
                && getCategory() == that.getCategory();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getProduct_type(), getCapacity(), getCategory());
    }

    @Override
    public String toString() {
        return "GeneratorsTypesDTO{" +
                "id=" + id +
                ", product_type='" + product_type + '\'' +
                ", capacity='" + capacity + '\'' +
                ", category=" + category +
                '}';
    }
}
