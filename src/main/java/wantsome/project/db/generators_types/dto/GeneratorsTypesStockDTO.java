package wantsome.project.db.generators_types.dto;

import java.util.Objects;

public class GeneratorsTypesStockDTO {
    private final String product_type;
    private final Category category;
    private final int stock;

    public GeneratorsTypesStockDTO(String product_type, Category category, int stock) {
        this.product_type = product_type;
        this.category = category;
        this.stock = stock;
    }

    public String getProduct_type() {
        return product_type;
    }

    public Category getCategory() {
        return category;
    }

    public int getStock() {
        return stock;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GeneratorsTypesStockDTO)) return false;
        GeneratorsTypesStockDTO that = (GeneratorsTypesStockDTO) o;
        return getStock() == that.getStock() && Objects.equals(getProduct_type(), that.getProduct_type()) && getCategory() == that.getCategory();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getProduct_type(), getCategory(), getStock());
    }

    @Override
    public String toString() {
        return "GeneratorsTypesStockDTO{" +
                "product_type='" + product_type + '\'' +
                ", category=" + category +
                ", stock=" + stock +
                '}';
    }
}
