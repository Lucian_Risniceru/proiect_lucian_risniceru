package wantsome.project.db.generators_types.dto;

public enum Category {
    LOW_COST("Low Cost"),
    PREMIUM("Premium");

    private final String label;

    Category(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
