package wantsome.project.db.users.dao;

import wantsome.project.db.DbManager;
import wantsome.project.db.users.dto.Department;
import wantsome.project.db.users.dto.UsersDTO;
import wantsome.project.db.users.dto.UsersTopDTO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UsersDAO {

    public static Optional<UsersDTO> load(int id) {

        String SQL = "SELECT * FROM USERS WHERE id = ?;";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(SQL)) {

            ps.setInt(1, id);

            try (ResultSet rs = ps.executeQuery()) {
                return rs.next() ? Optional.of(extractUserFromResult(rs)) :
                        Optional.empty();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error loading user" + e.getMessage());

        }
    }

    public static List<UsersDTO> loadAllUsers() {

        String sql = "SELECT * FROM USERS;";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            List<UsersDTO> usersDTO = new ArrayList<>();
            while (rs.next()) {

                usersDTO.add(
                        new UsersDTO(
                                rs.getInt("ID"),
                                rs.getString("FIRST_NAME"),
                                rs.getString("LAST_NAME"),
                                rs.getString("EMAIL"),
                                rs.getString("PHONE"),
                                Department.valueOf(rs.getString("DEPARTMENT")),
                                rs.getString("USERNAME"),
                                rs.getString("PASSWORD")));
            }
            return usersDTO;
        } catch (SQLException e) {
            throw new RuntimeException("Error loading USERS: " + e.getMessage());
        }
    }

    public static void insertNewUser(UsersDTO user) {

        String SQL = "INSERT INTO USERS (FIRST_NAME,\n" +
                "LAST_NAME,\n" +
                "EMAIL,\n" +
                "PHONE,\n" +
                "DEPARTMENT,\n" +
                "USERNAME,\n" +
                "PASSWORD) \n" +
                "VALUES (?,?,?,?,?,?,?);";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(SQL)) {


            ps.setString(1, user.getFirst_name());
            ps.setString(2, user.getLast_name());
            ps.setString(3, user.getEmail());
            ps.setString(4, user.getPhone());
            ps.setString(5, user.getDepartment().name());
            ps.setString(6, user.getUsername());
            ps.setString(7, user.getPassword());

            ps.execute();

        } catch (SQLException e) {
            String details = e.getMessage().contains("UNIQUE constraint failed: USERNAME") ?
                    "a USER with same USERNAME already exists" :
                    e.getMessage();
            throw new RuntimeException("Error saving new USER: " + details);
        }
    }

    public static void updatePersonalInfo(UsersDTO user) {
        String SQL = "UPDATE USERS \n" +
                "SET FIRST_NAME = ?,\n" +
                "\tLAST_NAME =?,\n" +
                "\tEMAIL =?,\n" +
                "\tPHONE =?,\n" +
                "\tUSERNAME = ?,\n" +
                "\tPASSWORD = ?\n" +
                "WHERE ID = ?;";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(SQL)) {

            ps.setString(1, user.getFirst_name());
            ps.setString(2, user.getLast_name());
            ps.setString(3, user.getEmail());
            ps.setString(4, user.getPhone());
            ps.setString(5, user.getUsername());
            ps.setString(6, user.getPassword());
            ps.setInt(7, user.getId());

            ps.execute();

        } catch (SQLException e) {
            String details = e.getMessage().contains("UNIQUE constraint failed: USERNAME") ?
                    "USERNAME already exists" :
                    e.getMessage();
            throw new RuntimeException("Error updating user personal information's: " + details);
        }
    }

    public static void updateUserAllInfo(UsersDTO user) {
        String SQL = "UPDATE USERS \n" +
                "SET FIRST_NAME = ?,\n" +
                "\tLAST_NAME =?,\n" +
                "\tEMAIL =?,\n" +
                "\tPHONE =?,\n" +
                "\tDEPARTMENT = ?,\n" +
                "\tUSERNAME = ?,\n" +
                "\tPASSWORD = ?\n " +
                "WHERE id =?;";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(SQL)) {

            ps.setString(1, user.getFirst_name());
            ps.setString(2, user.getLast_name());
            ps.setString(3, user.getEmail());
            ps.setString(4, user.getPhone());
            ps.setString(5, user.getDepartment().name());
            ps.setString(6, user.getUsername());
            ps.setString(7, user.getPassword());
            ps.setInt(8, user.getId());

            ps.execute();

        } catch (SQLException e) {
            String details = e.getMessage().contains("UNIQUE constraint failed: USERNAME") ?
                    "USERNAME already exists" :
                    e.getMessage();
            throw new RuntimeException("Error updating user personal information's: " + details);
        }
    }


    public static void deleteUser(int id) {

        String sql = "DELETE FROM USERS WHERE ID= ?;";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setInt(1, id);

            ps.execute();

        } catch (SQLException e) {
            throw new RuntimeException("Error deleting USER " + id + ": " + e.getMessage());
        }
    }

    public static Optional<UsersDTO> loadUserDetails(String username) {

        String SQL = "SELECT * FROM USERS\n" +
                "WHERE USERNAME = ?;";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(SQL)) {

            ps.setString(1, username);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    UsersDTO user = extractUserFromResult(rs);
                    return Optional.of(user);
                }
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error loading user" + e.getMessage());

        }
    }

    private static UsersDTO extractUserFromResult(ResultSet rs) throws SQLException {
        return new UsersDTO(
                rs.getInt("id"),
                rs.getString("first_name"),
                rs.getString("last_name"),
                rs.getString("email"),
                rs.getString("phone"),
                Department.valueOf(rs.getString("department")),
                rs.getString("username"),
                rs.getString("password"));
    }

    public static List<UsersTopDTO> topSeller() {
        String SQL = "SELECT USERS.FIRST_NAME, USERS.LAST_NAME,\n" +
                "(SELECT COUNT(*) FROM GENERATORS\n" +
                "WHERE GENERATORS.PRODUCT_STATUS ='READY_FOR_SHIPMENT'\n" +
                "AND USERS.ID =GENERATORS.USER_ID ) AS 'SOLD'\n" +
                "FROM USERS\n" +
                "ORDER BY SOLD DESC;";
        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(SQL);
             ResultSet rs = ps.executeQuery()) {
            List<UsersTopDTO> topSellers = new ArrayList<>();
            while (rs.next()) {
                topSellers.add(new UsersTopDTO(
                        rs.getString("first_name"),
                        rs.getString("last_name"),
                        rs.getInt("sold")));

            }
            return topSellers;
        } catch (SQLException e) {
            throw new RuntimeException("Error loading users: " + e.getMessage());
        }
    }
}

