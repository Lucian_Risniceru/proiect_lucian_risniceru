package wantsome.project.db.users.dto;

public enum Department {
    SALES("Sales"),
    PRODUCTION("Production"),
    ADMIN("Admin");

    private final String label;

    Department(String label) {
        this.label = label;
    }


    public String getLabel() {
        return label;
    }
}

