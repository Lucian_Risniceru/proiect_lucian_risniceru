package wantsome.project.db.users.dto;

import java.util.Objects;

public class UsersDTO {
    private final int id;
    private final String first_name;
    private final String last_name;
    private final String email;
    private final String phone;
    private final Department department;
    private final String username;
    private final String password;

    public UsersDTO(int id, String first_name
            , String last_name, String email
            , String phone, Department department
            , String username, String password) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.phone = phone;
        this.department = department;
        this.username = username;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public Department getDepartment() {
        return department;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UsersDTO)) return false;
        UsersDTO usersDTO = (UsersDTO) o;
        return getId() == usersDTO.getId()
                && getFirst_name().equals(usersDTO.getFirst_name())
                && getLast_name().equals(usersDTO.getLast_name())
                && getEmail().equals(usersDTO.getEmail())
                && getPhone().equals(usersDTO.getPhone())
                && getDepartment().equals(usersDTO.getDepartment())
                && getUsername().equals(usersDTO.getUsername())
                && getPassword().equals(usersDTO.getPassword());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getFirst_name()
                , getLast_name(), getEmail(), getPhone()
                , getDepartment(), getUsername(), getPassword());
    }

    @Override
    public String toString() {
        return "UsersDTO{" +
                "id=" + id +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", department='" + department + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
