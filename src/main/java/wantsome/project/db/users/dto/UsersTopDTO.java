package wantsome.project.db.users.dto;

import java.util.Objects;

public class UsersTopDTO {
    private final String first_name;
    private final String last_name;
    private final int sold;


    public UsersTopDTO(String first_name, String last_name, int sold) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.sold = sold;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public int getSold() {
        return sold;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UsersTopDTO)) return false;
        UsersTopDTO that = (UsersTopDTO) o;
        return getSold() == that.getSold() && Objects.equals(getFirst_name(), that.getFirst_name()) && Objects.equals(getLast_name(), that.getLast_name());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFirst_name(), getLast_name(), getSold());
    }

    @Override
    public String toString() {
        return "UsersTopDTO{" +
                "first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", sold=" + sold +
                '}';
    }
}
