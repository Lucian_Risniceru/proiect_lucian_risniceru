package wantsome.project.db;

import wantsome.project.db.generators.dao.GeneratorsDAO;
import wantsome.project.db.generators.dto.GeneratorsDTO;
import wantsome.project.db.generators_types.dao.GeneratorTypeDAO;
import wantsome.project.db.generators_types.dto.GeneratorsTypesDTO;
import wantsome.project.db.users.dao.UsersDAO;
import wantsome.project.db.users.dto.Department;
import wantsome.project.db.users.dto.UsersDTO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Statement;

import static wantsome.project.db.generators.dto.ProductStatus.*;
import static wantsome.project.db.generators.dto.ProductionPriority.*;
import static wantsome.project.db.generators_types.dto.Category.LOW_COST;
import static wantsome.project.db.generators_types.dto.Category.PREMIUM;

public class DbInitService {

    private static final String CREATE_GENERATORS_TABLE_SQL =
            "CREATE TABLE IF NOT EXISTS GENERATORS(" +
                    "ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "PRODUCT_SERIAL_NUMBER VARCHAR(100) NOT NULL UNIQUE," +
                    "GENERATOR_TYPE_ID INTEGER(3) NOT NULL REFERENCES GENERATORS_TYPES(ID)," +
                    "MANUFACTURING_DATE DATE," +
                    "USER_ID INTEGER REFERENCES USERS(ID)," +
                    "CUSTOMER VARCHAR(200)," +
                    "REMARK VARCHAR(500)," +
                    "PRODUCT_STATUS VARCHAR(100) CHECK(PRODUCT_STATUS IN('" + IN_PROGRESS + "','" + STOCK + "','" + RESERVED + "','" + READY_FOR_SHIPMENT + "','" + SHIPPED + "'))," +
                    "RESERVATION_DATE DATE," +
                    "SHIPPING_DATE DATE," +
                    "PRODUCTION_PRIORITY VARCHAR(100) CHECK(PRODUCTION_PRIORITY IN('" + HIGH + "','" + MEDIUM + "','" + LOW + "'))" +
                    ");";


    private static final String CREATE_GENERATORS_TYPE_TABLE_SQL =
            "CREATE TABLE IF NOT EXISTS GENERATORS_TYPES(" +
                    "ID INTEGER  PRIMARY KEY AUTOINCREMENT," +
                    "PRODUCT_TYPE VARCHAR (100) NOT NULL UNIQUE," +
                    "CAPACITY VARCHAR (50) NOT NULL," +
                    "CATEGORY VARCHAR(100) NOT NULL" +
                    ");";

    private static final String CREATE_USERS_TABLE_SQL =
            "CREATE TABLE IF NOT EXISTS USERS(" +
                    "ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "FIRST_NAME VARCHAR (145) NOT NULL," +
                    "LAST_NAME VARCHAR (145) NOT NULL," +
                    "EMAIL VARCHAR (145) ," +
                    "PHONE VARCHAR (12) ," +
                    "DEPARTMENT VARCHAR(50)NOT NULL," +
                    "USERNAME VARCHAR (145) NOT NULL UNIQUE," +
                    "PASSWORD VARCHAR (16) NOT NULL" +
                    ");";


    public static void initDatabase() {
        createMissingTables();
        createDefaultGeneratorsTypes();
        createAdminUser();
        createDefaultGenerator();
    }

    public static void createMissingTables() {
        try (Connection conn = DbManager.getConnection();
             Statement st = conn.createStatement()) {

            st.execute(CREATE_GENERATORS_TYPE_TABLE_SQL);
            st.execute(CREATE_USERS_TABLE_SQL);
            st.execute(CREATE_GENERATORS_TABLE_SQL);

        } catch (SQLException e) {
            throw new RuntimeException("Error creating missing tables: " + e.getMessage());
        }
    }

    public static void createDefaultGeneratorsTypes() {
        if (GeneratorTypeDAO.loadAllGeneratorsTypes().isEmpty()) {
            GeneratorTypeDAO.insertGeneratorType(new GeneratorsTypesDTO(1, "GTQA 20K-AI", "20 KVA", LOW_COST));
            GeneratorTypeDAO.insertGeneratorType(new GeneratorsTypesDTO(2, "GTQA 25K-AI", "25 KVA", LOW_COST));
            GeneratorTypeDAO.insertGeneratorType(new GeneratorsTypesDTO(3, "GTQA 30K-AI", "30 KVA", LOW_COST));
            GeneratorTypeDAO.insertGeneratorType(new GeneratorsTypesDTO(4, "GTQA 40K-AI", "40 KVA", LOW_COST));
            GeneratorTypeDAO.insertGeneratorType(new GeneratorsTypesDTO(5, "GTQA 50K-AI", "50 KVA", LOW_COST));
            GeneratorTypeDAO.insertGeneratorType(new GeneratorsTypesDTO(6, "GTQA 55K-AI", "55 KVA", LOW_COST));
            GeneratorTypeDAO.insertGeneratorType(new GeneratorsTypesDTO(7, "GTQA 60K-AI", "60 KVA", LOW_COST));
            GeneratorTypeDAO.insertGeneratorType(new GeneratorsTypesDTO(8, "GTQA 75K-AI", "75 KVA", LOW_COST));
            GeneratorTypeDAO.insertGeneratorType(new GeneratorsTypesDTO(9, "GTQA 100K-AI", "100 KVA", LOW_COST));
            GeneratorTypeDAO.insertGeneratorType(new GeneratorsTypesDTO(10, "GTQA 110K-AI", "110 KVA", LOW_COST));
            GeneratorTypeDAO.insertGeneratorType(new GeneratorsTypesDTO(11, "GTQA 150K-AI", "150 KVA", LOW_COST));
            GeneratorTypeDAO.insertGeneratorType(new GeneratorsTypesDTO(12, "GTQA 200K-AI", "200KVA", LOW_COST));
            GeneratorTypeDAO.insertGeneratorType(new GeneratorsTypesDTO(13, "GTQA 20I-AI", "20 KVA", PREMIUM));
            GeneratorTypeDAO.insertGeneratorType(new GeneratorsTypesDTO(14, "GTQA 30I-AI", "30 KVA", PREMIUM));
            GeneratorTypeDAO.insertGeneratorType(new GeneratorsTypesDTO(15, "GTQA 40I-AI", "40 KVA", PREMIUM));
            GeneratorTypeDAO.insertGeneratorType(new GeneratorsTypesDTO(16, "GTQA 50I-AI", "50 KVA", PREMIUM));
            GeneratorTypeDAO.insertGeneratorType(new GeneratorsTypesDTO(17, "GTQA 60I-AI", "60 KVA", PREMIUM));
            GeneratorTypeDAO.insertGeneratorType(new GeneratorsTypesDTO(18, "GTQA 80I-AI", "80 KVA", PREMIUM));
            GeneratorTypeDAO.insertGeneratorType(new GeneratorsTypesDTO(19, "GTQA 100I-AI", "100 KVA", PREMIUM));
            GeneratorTypeDAO.insertGeneratorType(new GeneratorsTypesDTO(20, "GTQA 125I-AI", "125 KVA", PREMIUM));
            GeneratorTypeDAO.insertGeneratorType(new GeneratorsTypesDTO(21, "GTQA 160I-AI", "160 KVA", PREMIUM));
            GeneratorTypeDAO.insertGeneratorType(new GeneratorsTypesDTO(22, "GTQA 175I-AI", "175 KVA", PREMIUM));
        }
    }

    public static void createAdminUser() {
        if (UsersDAO.loadAllUsers().isEmpty()) {
            UsersDAO.insertNewUser(new UsersDTO(1, "AdminFn", "AdminLn", null, null, Department.ADMIN, "Admin", "Admin"));
            UsersDAO.insertNewUser(new UsersDTO(2, "SalesFn", "SalesLn", null, null, Department.SALES, "Sales", "Sales"));
            UsersDAO.insertNewUser(new UsersDTO(3, "ProductionFn", "ProductionLn", null, null, Department.PRODUCTION, "Production", "Production"));
        }
    }

    public static void createDefaultGenerator() {
        if (GeneratorsDAO.loadFullDetailsGenerators().isEmpty()) {
            GeneratorsDAO.insertGenerator(new GeneratorsDTO(0, "GTQA.000.2022", 1, new Date(System.currentTimeMillis()), 1, null, null, RESERVED, null, null, LOW));
            GeneratorsDAO.insertGenerator(new GeneratorsDTO(1, "GTQA.001.2022", 2, new Date(System.currentTimeMillis()), 2, null, null, STOCK, new Date(System.currentTimeMillis()), null, LOW));
            GeneratorsDAO.insertGenerator(new GeneratorsDTO(2, "GTQA.002.2022", 3, new Date(System.currentTimeMillis()), 3, null, null, IN_PROGRESS, new Date(System.currentTimeMillis()), null, LOW));
            GeneratorsDAO.insertGenerator(new GeneratorsDTO(3, "GTQA.003.2022", 4, new Date(System.currentTimeMillis()), 2, null, null, SHIPPED, new Date(System.currentTimeMillis()), new Date(System.currentTimeMillis()), LOW));
            GeneratorsDAO.insertGenerator(new GeneratorsDTO(4, "GTQA.004.2022", 5, new Date(System.currentTimeMillis()), 2, null, null, READY_FOR_SHIPMENT, new Date(System.currentTimeMillis()), new Date(System.currentTimeMillis()), LOW));
        }
    }
}

