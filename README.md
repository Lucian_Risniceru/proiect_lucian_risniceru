## Wantsome - Project Stock Control

### 1. Description

The premises of the project are to develop an online system with restricted access based on user type, for tracking
product stocks, adding new products in stock and creating reports based on the number of products sold. The main
objective are to facilitate the possibility of offering the finished products to the customer more easy, to follow the
performances of the sales agents and to eliminate the sale of the same product several times.

The access in application is made by entering the username and password, the values are validated in database and if the
user is not registered in database it will be announced by a message that the access is denied.

![login](materials/Login_form.png "Login Example")

The standard users types are:

- <b>Production</b>
- <b>Sales</b>
- <b>Administrator</b>

---

### 2. Functionality

__Production user__

As a production user, will have the following view:

![production_view](materials/Production_view.PNG "Production view")

The main responsibilities are:

- to add new products in production process by selecting the Add Generator route, a form will be opened for filling all
  the details needed

![add_product](materials/Production_add_generator.PNG "Add new product in process")

- to establish the manufacturing date by selecting the date when the product will be finished in production;
- to take care with product priority requested from sales department;
- to declare the products' status shipped when the product will be delivered.

For an easy follow up and access, production user has the possibility to sort the products by priority: High, Medium,
Low, All, to order them by priority and manufacturing date, selecting from drop down list on the top of the data table
the option needed.

If is a new product to produce the user has the possibility to add a new type of product by selecting on Add Type form
Generators Types page a special form will be opened for filling the details with the new product

![add_type](materials/Add_Generator_Type.PNG "Adding new generator")

__Sales user__

Has the access restricted for adding new generators in production process or to add new types of products. He can view
the status of all generators and for an easy access he has the possibility to sort them by status: Stock, Reserved,
Ready for shipment or to order by product type and manufacturing date. If he has a new buyer or possible buyer, the
seller can reserve the product till the contract will be signed or paid, by changing the status value to "RESERVED". At
this moment nobody can sell or reserve that product, the access is restricted fot other users. To have a good tracking
of the status the application will register automatically the first name and last name of the seller, information taken
from the session.

![stock_view](materials/Stock_page.PNG "Stock Page")

The user has also the possibility to update his personal information by clicking on his name from the first page and a
special form will be displayed with all data registered.

__Administrator user__

Has full access to all pages and the main duty is to add new users, this will keep database more secure!
![users_list](materials/Users_list.PNG "Users List")

The application has a special page with Reports, all users has access on it, here we have displayed a list with top of
the seller, out of stock products and a list with all products sold. Important information for all departments!

![reports](materials/Reports.PNG "Reports")

---

### 3. Setup

No setup needed, just start the application. If the database is missing (like on first startup), it will create a new
database (of type SQLite, stored in a local file named 'StockControlGTQA.db'), and use it to save the future data.

Once started, access it with a web browser at: http://localhost:8080

---

### 4. Technical details

__Technologies__

- main code is written in Java (version 11)
- it uses [SQLite](https://www.sqlite.org/), a small embedded database, for its persistence, using SQL and JDBC to
  access it from Java code
- it uses [Javalin](https://javalin.io/) micro web framework
- it uses [Velocity](https://velocity.apache.org/) templating engine, to separate the UI code from Java code; UI code
  consists of basic HTML and CSS code

- java code is organized in packages by its role, on layers:
  - db - database part, including DTOs and DAOs, as well as the code to init and connect to the db
  - ui - code related to the interface/presentation layer

- web resources are found in `main/resources` folder:
  - under `/public` folder - static resources to be served by the web server directly (images, css files)
  - all other (directly under `/resources`) - the Velocity templates

---

### 5. Future plans

- export/import data using CVS files
- visual graphics on reports using JavaScript library
  [Google Charts](https://developers.google.com/chart)
- notifications on particular status change
